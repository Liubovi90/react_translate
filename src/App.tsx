import React, { FC, useContext } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.scss';

import Homepage from './pages/Homepage';
import { LangContext } from './context/lang';

const App: FC = () => {
  const { dispatch: { translate }} = useContext(LangContext);
  
  return (
    <BrowserRouter basename="/react-multilang-website">
      <Switch>
        <Route path="/" exact>
          <Homepage translate={translate} />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;